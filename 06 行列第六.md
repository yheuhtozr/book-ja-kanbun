# 行列第六

森しん=森しんタル矛ぼう=戟げき〔4てんをつくやうなやりぶすま〕。洸くゎう=洸くゎうタル〔2いさましい〕武もの_夫のふ。陣ちん〔たいれつ〕者（は）兵へい之（の）列れつ也（なり）。鱗りん=鱗りんタル孔こう=方はう〔4ぎらぎらひかるコイン〕。穰じゃう=穰じゃうタル青せい=蚨ふ〔4じゃらじゃらいふおかね〕。吊てう〔さしぜに〕者（は）錢ぜに之（の）列れつ也（なり）。靡び=靡びタル鄭てい=聲せい〔4アゲアゲのポップソング〕。丁たう=丁たうタル清せい=響きゃう〔4いやしのＢＧＭ〕。曲きょく者（は）音おと之（の）列れつ也（なり）。浩かう=浩かうタリ汪わう=洋やうタリ〔4あふれるインスピレーション〕。揚やう=揚やうタル義ぎ=氣き〔4さうだいなロマン〕。文ぶん=章しゃう者（は）字じ之（の）列れつ也（なり）。天てん=下か之（の）物もの既みな眾おおクシテ。或あるいハ不（ず）0知しラ2幾いく_何ばくカヲ1。或あるいハ不（ず）0可べカラ2勝あゲテ數かぞフ1〔4かぞへきれない〕。皆みな以もっテ2列れつ=法はふヲ1法はふス〔きそくだてる〕之これヲ。數すう=列れつ列つらネ0數すうヲ。言げん=列れつ列つらネ0言げんヲ。爻かう=列れつ列つらネ0爻かうヲ。列れつ=列れつ列つらヌ0列れつヲ。罔なシ不（ざ）ル裁さばクニ0物ものヲ而器うつはシ〔4ものにみあったかたちをとる〕。序ついデルニ0物ものヲ而次ついでシ〔4じゅんじょよくならぶ〕。載のセルニ0物ものヲ而厚あつカラ〔4いくらでもものがはひる〕。是かク謂いフ2之これヲ列れつト1。

維これ編へん=程ていニテ〔2プログラミング〕用もちヰル0列れつヲ之（の）法はふ。或あるいハ入いレ0之これニ。或あるいハ取とル0之これヨリ。入いルル法はふニ二ふたツアリ。曰いはク`充`。曰いはク`銜`。取とル法はふニ三みツアリ。曰いはク`其一`。曰いはク`其餘`。曰いはク`之長`。

列れつ之（の）初そ=創さう〔2さくせいじ〕。空からニシテ無なシ一いち=物ぶつ。`充`者（は）。一いつ=一いつ添そヘル2於其そノ後うしろニ1也（なり）。

```
吾有一列。名之曰「甲」。充「甲」以三。充「甲」以五。
```

當あタリ2是こノ時ときニ1。「甲かふ」之（の）列れつ乃すなはチ有あリ2二に=數すう1。曰いはク三さん。曰いはク五ご。或あるヒト問とヒテ曰いはク。欲ほっス2一いっ=舉きょニシテ而充じゅうセンコトヲ10之これニ。可かナル乎（か）ト。曰いはク。可か也（なり）ト。

```
吾有一列。名之曰「乙」。充「乙」以二。以九。以四。以二十二。
```

故ゆゑニ是こノ「甲かふ」ニ有あリ2二に=數すう1。「乙いつ」ニ有あリ2四し=數すう1。今いま欲ほっス3聯れん2=合がふセンコトヲ甲かふ=乙いつヲ1。令しメ2甲かふ之（の）後うしろヲシテ為（た）リ0乙いつ。乙いつ之（の）前まへヲシテ為（た）ラ10甲かふ。併あはセテ2兩りゃう=列れつヲ1成なス2一いち=列れつト1。乃すなはチ用もちヰル2`銜`句くヲ1。

```
銜「甲」以「乙」。名之曰「丙」。
```

遑なク0論ろんズル2列れつ之（の）若じゃっ=干かんヲ1〔2いくつあるか〕。亦まタ可べキコト2一いっ=舉きょニシテ而銜かんスル10之これヲ如ごとシ0是かクノ。

```
銜「甲」以「乙」。以「丙」。以「丁」。名之曰「戊」。
```

既すでニ見みレバ2入いルル法はふヲ1。復まタ聞きカン2取とル法はふヲ1。今いま取とリ2其そノ列れつ之（の）首しゅ=物ぶつ。次じ=物ぶつヲ1。觀み0之これヲ用もちヰン0之これヲ。書かクコト0之これヲ如ごとシ0是かクノ。

```
夫「甲」之一。書之。
夫「甲」之二。書之。
夫「乙」之四。加其以四十五。書之。
```

欲ほっスレバ0易かヘンコトヲ2列れつ=中ちう之（の）物ものヲ1。乃すなはチ稍やヤ變かヘテ2前ぜん=章しゃうノ`昔之`ノ句くヲ1。書かクコト如ごとシ0是かクノ。

```
昔之「甲」之一者。今五是矣。
昔之「乙」之三者。今「丙」之四是矣。
```

列れつ之（の）長ちゃう=短たんニ無なシ0定さだメ。或あるいハ寥れう=寥れう〔2たった〕二に=三さん=物ぶつ。乃ない=至し成なシ0千せんヲ上のぼル0萬まんニ。然しかルニ亦まタ毋（ず）0須もちヰ2另れい=記きヲ1〔2ひかへておく〕。時ときニ可べキz以もっテ2`之長`ノ句くヲ1得うx0之これヲ也（なり）。既すでニ知しレバ2其そノ長ちゃうヲ1。以もっテ2循じゅん=環くゎんヲ1次し=第だいニ觀みン2一いち=列れつ之（の）物ものヲ1〔なかみ〕。易やすキ矣（かな）。

```
夫「甲」之長。書之。

有數一。名之曰「乙」。
恆為是。若「乙」大於「甲」之長者。乃止也。
	夫「甲」之「乙」。書之。
加「乙」以一。昔之「乙」者。今其是矣。云云。
```

列れつ既すでニ能よく增ふヤシテ而長ながクスレバ0之これヲ。亦まタ可べシ2分わケテ以もっテ短みじかクス10之これヲ。`其餘`者（は）。棄すテ2其そノ第だい=一いちナルヲ1而留とどムル2其そノ他ほかヲ1也（なり）。

```
夫「甲」之其餘。名之曰「乙」。
```

使しメバ3「甲かふ」ヲシテ有あラ2一いち/二に/三さん1者。「乙いつ」ニ乃すなはチ有あリ2二に/三さん1。使しメバ3「甲かふ」ヲシテ有あラ2二に/四し/六ろく/八はち1者。「乙いつ」ニ乃すなはチ有あリ2四し/六ろく/八はち1。是こレ謂いフ2`其餘`ト1也（なり）。

或あるヒト問とヒテ曰いはク。先まヅ充じゅうスニ0之これニ以もっテシ0數すうヲ。後のちニ充じゅうスニ0之これニ以もっテス0言げんヲ。或あるいハ復まタ充じゅうスニ0之これニ以もっテス0爻かうヲ。可かナル乎（か）ト。曰いはク。不ふ=可か也（なり）。列れつ初そ=創さうハ。其そノ類るい〔かた〕無なシ0定さだメ。然しかレドモ既すでニ充じゅうスレバ輒すなはチ知しル〔くべつされる〕。自（より）ハ是これ不（ざ）ル0得え2混こん=淆かうスルヲ1耳（のみ）ト。

又また列れつニ有あレバ2異い=名めい1者。其そノ實じつ或あるいハ同おなジ。牽ひキテ0一いちヲ而二に動うごク。例れいニ曰いはク。

```
吾有一列。名之曰「甲」。充「甲」以一。以二。以三。
吾有一列。曰「甲」。名之曰「乙」。
昔之「乙」之一者。今四是矣。
夫「甲」。書之。
```

乃すなはチ得うル0「甲かふ」ヲ者（こと）。`四。二。三。`也（なり）。唯たダ易かヘテ0「乙いつ」ノミ而殃とがめ及およブ0「甲かふ」ニモ。不（ざ）ル2亦まタ謬あやまリナラ1乎（か）。曰いはク。非あらザル也（なり）。蓋けだシ「甲かふ」「乙いつ」ノ所ところハ0指さス。同おなジ一いち=列れつ也（なり）。名な雖いへどモ0二ふたツト則すなはチ實じつハ0〔なかみ〕一ひとツ也（なり）ト。問とヒテ曰いはク。欲ほっスレバ0令しメンコトヲ3其それヲシテ實じつ亦まタ二ふたツシテ。互たがヒニ不（ざ）ラ2相あひ干かんセ1。當まさニ2+如い_何かんスベキカト1。曰いはク。當まさに2+如ごとクスベシト10是かクノ。

```
吾有一列。名之曰「乙」。
凡「甲」中之「元」。充「乙」以「元」也。
```

漢かんノ張ちゃう=衡かう字あざな平へい=子し。全せん=才さい=〔2オールマイティ〕人じん也（なり）。文ぶんニ〔さくひん〕有あリ2四し=愁しう=詩し1。其そノ詩し風ふう=流りう婉ゑん=轉てんナリ〔4げいじゅつてきでリズミカル〕。上うへハ承うケ2屈くっ=子し〔2こだいのしじん〕之（の）遺い=義ぎヲ1。下したハ啟ひらク2七しち=言ごん〔2しのけいしき〕之（の）濫らん=觴しゃうヲ1。今いま假かリテ2其そノ詩しヲ1以もっテ示しめサン2列れつ之（の）用ようヲ1。

其そノ詩し四し=章しゃうアリ〔れん〕。句く多おほク復ふく=沓たふス〔2リフレイン〕。若ごとシ2「我所思兮在〔5あひたいひとが――にゐる〕」云うん=云ぬんハ皆みな同おなジクシテ。唯たダ「太たい=山ざん」「桂けい=林りん」。「金きん=錯さく=刀たう」「琴きん=琅らう=玕かん」ノ諸しょ=辭し固もとヨリ異ことナル1。故ゆゑニ先まヅ入いレテ2其そノ變へん=辭しヲ於列れつ=中ちうニ1待まツ0用ようヲ也（なり）。

```
吾有八列。名之曰「其所」。曰「所難」。曰「其方」。曰「所沾」。曰「所贈」。曰「所報」。曰「所感」。曰「所傷」。
充「其所」以「「太山」」以「「桂林」」以「「漢陽」」以「「雁門」」。
充「所難」以「「樑父艱」」以「「湘水深」」以「「隴阪長」」以「「雪雰雰」」。
充「其方」以「「東」」以「「南」」以「「西」」以「「北」」。
充「所沾」以「「翰」」以「「襟」」以「「裳」」以「「巾」」。
充「所贈」以「「金錯刀」」以「「琴琅玕」」以「「貂襜褕」」以「「錦繡段」」。
充「所報」以「「英瓊瑤」」以「「雙玉盤」」以「「明月珠」」以「「青玉案」」。
充「所感」以「「逍遙」」以「「惆悵」」以「「踟躕」」以「「增嘆」」。
充「所傷」以「「勞」」以「「傷」」以「「紆」」以「「惋」」。
```

未いまダ0+示しめサザルニ2其そノ詩しヲ1。請こフ先まヅ以もっテ一いっ2=觀くゎんセン諸しょ=列れつヲ1。`凡「列」中之「元」`者（は）。亦まタ循じゅん=環くゎん也（なり）。是こノ句く見みユ2於前ぜん=章しゃうニ1。斯かノ人ひとノ〔2おもひびと〕所ところ0居をル。斯かノ人ひとノ所ところ0遺のこス。藉かリテ0此これニ以もっテ列つらヌ0之これヲ。

```
凡「其所」中之「地」。
	夫「地」。書之。
云云。
吾有一言。曰「「此皆有所思之地也」」。書之。

凡「所贈」中之「寶」。
	夫「寶」。書之。
云云。
吾有一言。曰「「此皆美人之所贈也」」。書之。
```

乃すなはチ以もっテ0左ひだりヲ書かク2張ちゃう=子し之（の）詩しヲ1。其そノ循じゅん=環くゎん之（の）體てい〔ほんぶん〕。或あるいハ言いヒ2復ふく=沓たふヲ1。或あるいハ取とル2變へん=辭しヲ於諸しょ=列れつヨリ1。終つひニ一いっ=章しゃう一いっ=併ぺいニ〔2いっき〕書かク0之これヲ。

```
有數一。名之曰「章」。
夫「其所」之長。為是其遍。
	吾有一言。曰「「我所思兮在」」。夫「其所」之「章」。
	吾有一言。曰「「。欲往從之」」。夫「所難」之「章」。
	吾有一言。曰「「。側身」」。夫「其方」之「章」。
	吾有一言。曰「「望涕沾」」。夫「所沾」之「章」。
	吾有一言。曰「「。美人贈我」」。夫「所贈」之「章」。
	吾有一言。曰「「。何以報之」」。夫「所報」之「章」。
	吾有一言。曰「「。路遠莫致倚」」。夫「所感」之「章」。
	吾有一言。曰「「。何爲懷憂心煩」」。夫「所傷」之「章」。
	書之。
	加「章」以一。昔之「章」者。今其是矣。
云云。
```

乃すなはチ得うルコト2原げん=詩しヲ1如ごとシ0是かクノ。


```
我所思兮在太山。欲往從之樑父艱。側身東望涕沾翰。美人贈我金錯刀。何以報之英瓊瑤。路遠莫致倚逍遙。何爲懷憂心煩勞
我所思兮在桂林。欲往從之湘水深。側身南望涕沾襟。美人贈我琴琅玕。何以報之雙玉盤。路遠莫致倚惆悵。何爲懷憂心煩傷
我所思兮在漢陽。欲往從之隴阪長。側身西望涕沾裳。美人贈我貂襜褕。何以報之明月珠。路遠莫致倚踟躕。何爲懷憂心煩紆
我所思兮在雁門。欲往從之雪雰雰。側身北望涕沾巾。美人贈我錦繡段。何以報之青玉案。路遠莫致倚增嘆。何爲懷憂心煩惋
```

其そノ詩し晉しん_人ひと傅ふ=玄げん。宋そう_人ひと張ちゃう=載さい。近きん=人じん魯ろ=迅じん皆みな嘗かつテ為なス2擬ぎ=作さくヲ1。請こフ吾ご=子し〔2みなさん〕擇えらビ2己おのれノ所ところヲ10好このム。試こころミニ易かヘテ2前ぜん=文ぶんノ諸しょ=列れつヲ1以もっテ得えン2其そノ詩しヲ1。

列れつ=列れつ〔2にじげんはいれつ〕者（は）。載のセル列れつヲ之（の）列れつ也（なり）。縱しょう=橫くゎうニ如ごとク2方はう=陣ぢんノ1然しかリ。如ごとク2棋き=局きょくノ1〔2ごばん〕然しかリ。如ごとク2田でん=壠ろうノ1〔2あぜでしきられたたんぼ〕然しかリ。

尚しゃう=書しょ正せい=義ぎニ〔4ほんのなまへ〕曰いはク。天てん與あたフ2禹うニ〔でんせつのわう〕洛らく=出しゅつ=書しょヲ1。神しん=龜き負おヒテ0文あやヲ〔もやう〕而出いデ。列れっス2於背せニ1。有あリ0數すう至いたル2於九きうニ1。禹う遂つひニ因よリテ而第ていシ0〔せいりし〕之これヲ。以もっテ成なスト2九きう=類るいト1。朱しゅ=熹き以もっテ0之これヲ為なス2九きう=宮きう=數すうト1。奇きナル者（は）合あハセルニ2縱たて/橫よこ/斜ななめヲ1皆みな十じふ=五ごナリ。其そノ術じゅつ見みユ2於楊やう=輝きノ續しょく=古こ=摘てき=奇き=算さん=法ぱふニ1〔6ほんのなまへ〕。曰いはク九きう=子し〔かず〕斜ななメニ排ならブ。上しゃう=下か對たい=易えきシ〔2ひっくりかへし〕。左さ=右いう相あひ更かヘ。四し=維ゐ〔2よすみ〕挺てい=出しゅつス〔2つきださせる〕。戴いただキ0〔まうへになり〕九きうヲ履はク0〔ましたになる〕一いちヲ。左ひだり三さんニシテ右みぎ七しちナリ。二に/四し為（た）リ0肩かた。六ろく/八はち為（た）リ0足あし。五ご居をル2中ちう=央あうニ1。今いま入いレルコト2九きう=宮きう=數すうヲ於列れつ=列れつニ1如ごとシ0下したノ。

```
吾有一列。名之曰「行一」。充「行一」以四。以九。以二。
吾有一列。名之曰「行二」。充「行二」以三。以五。以七。
吾有一列。名之曰「行三」。充「行三」以八。以一。以六。
吾有一列。名之曰「九宮」。充「九宮」以「行一」以「行二」以「行三」。

凡「九宮」中之「行」。夫「行」。書之也。
```

然しかルニ是これ但たダ列つらネタルノミ2其そノ數すうヲ1。未いまダ2+嘗かつテ以もっテ0術じゅつヲ算さんセズ10之これヲ。假かレバ2楊やう=氏しノ妙めう=術じゅつヲ1。三さん=三さん=〔2３×３〕圖と。五ご=五ご=〔2５×５〕圖と。七しち=七しち=〔2７×７〕圖と。九きう=九きう=〔2９×９〕圖と。凡およソ其そノ橫くゎう=縱しょう為（た）ル2陽やう=數すう1〔2きすう〕者もの。皆みな一いっ=法ぱふニテ可べシ0得う。然しかレドモ楊やう=氏し固もとヨリ不（ず）z自みづかラ知しラy。猶なホ以もっテ2他た=法はふ1為なスコトヲx0之これヲ。今いま從したがヒ2其そノ洛らく=書しょノ簡かん=法ぱふニ1。編へん=程ていスルコト如ごとシ0是かクノ。注ちうシテ曰いはク。圖と之（の）創はじメ。苟かりそめニ以もっテ2循じゅん=環くゎんヲ1。盡ことごとク充じゅうスルハ0以もっテ0零れいヲ。蓋けだシ待まツ2後こう=算さんヲ1也（なり）。夫それ「廣くゎう〔おほきさ〕」者（は）。縱しょう=橫くゎう之（の）數すう也（なり）。聊いささカ〔ひとまず〕曰いヒ九きうト。以もっテ為なス2九きう=九きう=圖とヲ1。

```
有數九。名之曰「廣」。

吾有一列。名之曰「縱橫圖」。
為是「廣」遍。
	吾有一列。名之曰「行」。
	為是「廣」遍。充「行」以零也。
  充「縱橫圖」以「行」也。
```

既すでニ創つくレバ其そノ圖とヲ。乃すなはチ以もっテ2楊やう=氏し之（の）術じゅつヲ1遍へん2=算さんス〔2つぎつぎにもとめていく〕其そノ數すうヲ1。注ちうシテ曰いはク。「磔たく」者（は）。捺だっ=筆ぴつ〔2みぎはらひ〕也（なり）。自（より）左さ=上しゃう而右いう=下かス。「掠りゃく」者（は）。撇へっ=筆ぴつ〔2ひだりはらひ〕也（なり）。自（より）右いう=上しゃう而左さ=下かス。此こノ二に=子し者（は）。假かリテ2書しょ=家かノ語ごヲ1。所ゆ3_以ゑん〔2ためのもの〕定さだメル九きう=子し斜しゃ=排はい之（の）方はう=位ゐヲ1也（なり）。「勒ろく」者（は）。橫くゎう=筆ひつ〔2よこせん〕也（なり）。「努ど」者（は）。縱しょう=筆ひつ〔2たてせん〕也（なり）。此こノ二に=子し者（は）。所ゆ3_以ゑん化かス2斜しゃ=排はい之（の）方はう=位ゐヲ於正せい=排はい〔2ただしいはいち〕之（の）方はう=位ゐニ1也（なり）。然しかレドモ「勒ろく」「努ど」之の位くらゐ。或あるいハ出いヅ2於其そノ圖と之（の）界さかひヨリ1。遂つひニ以もっテ2求きう=餘よノ1法はふヲ。令しム2出いヅル0上うへニ者ものヲシテ下したニシ。出いヅル0下したニ者ものヲシテ上うへニシ。出いヅル0左ひだりニ者ものヲシテ右みぎニシ。出いヅル0右ひだりニ者ものヲシテ左ひだりニセ1。是こレ所いは_謂ゆる上しゃう=下か對たい=易えき。左さ=右いう相さう=更かうナル者（もの）也（なり）。乃すなはチ得う2「縱しゃう」「橫くゎう」ヲ1。於おいテ0是ここニ填てんシ2以もっテ「數すう」ヲ1。終つひニ得えン2其そノ圖とヲ1矣。

```
減「廣」以一。除其以二。名之曰「半」。

有數一。名之曰「數」。
乘「廣」以「廣」。為是其遍。
	減「數」以一。除其以「廣」。所餘幾何。名之曰「磔」。
	減「數」以「磔」。減其以一。除其以「廣」。名之曰「掠」。
	加「半」於「磔」。減其以「掠」。名之曰「勒」。
	減「半」於「磔」。加其以「掠」。名之曰「努」。
	加「勒」以「廣」。除其以「廣」。所餘幾何。加其以一。名之曰「橫」。
	加「努」以「廣」。除其以「廣」。所餘幾何。加其以一。名之曰「縱」。
	夫「縱橫圖」之「縱」。名之曰「行」。
	昔之「行」之「橫」者。今「數」是矣。
加「數」以一。昔之「數」者。今其是矣。云云。

凡「縱橫圖」中之「行」。夫「行」。書之也。
```

乃すなはチ得うルコト2九きう=九きう=圖とヲ1如ごとシ0是かクノ。其それ與（と）2楊やう=氏しノ書しょ=中ちうニ所ところ10載のスル不（ざ）ル0同おなジカラ者（は）。蓋けだシ作つくル2其そノ圖とヲ1之（の）法はふ甚はなはダ眾おほクシテ。所ところ0得うル亦まタ各おの〻有あリ0異ことナル。然しかシテ其それ之（の）為（た）ル2縱しょう=橫くゎう=圖と1者（は）一いつ〔おなじもの〕也（なり）。一いつナル者（とは）何なんゾ耶（や）。縱たて/橫よこ/斜ななめ皆みな三さん=百びゃく六ろく=十じふ=九きう也（なり）。或あるヒト問とヒテ曰いはク。何なにヲ以もっテカ知しルト0之これヲ。曰いはク。請こフ吾ご=子し試こころミニ作つくリ2一いち=術じゅつヲ1。循じゅん2=環くゎん遍へん3=歷れきセン〔2しらべつくす〕其そノ圖とヲ1。以もっテ驗あかしスル2吾わガ言げんノ之不（ざ）ルヲ10謬あやまラ也（なり）。

```
三十七。七十八。二十九。　七十。二十一。六十二。一十三。五十四。　五
　六　。三十八。七十九。　三十。七十一。二十二。六十三。一十四。四十六
四十七。　七　。三十九。　八十。三十一。七十二。二十三。五十五。一十五
一十六。四十八。　八　。　四十。八十一。三十二。六十四。二十四。五十六
五十七。一十七。四十九。　九　。四十一。七十三。三十三。六十五。二十五
二十六。五十八。一十八。　五十。　一　。四十二。七十四。三十四。六十六
六十七。二十七。五十九。　一十。五十一。　二　。四十三。七十五。三十五
三十六。六十八。一十九。　六十。一十一。五十二。　三　。四十四。七十六
七十七。二十八。六十九。　二十。六十一。一十二。五十三。　四　。四十五
```

又また有あリz傳つたハル2暹しゃ=羅むニ1〔2タイ〕法はふノ作つくルy陽やう=數すう=縱しょう=橫くゎう=圖とヲx。其そノ決けつ〔やりかた〕曰いはク。一はじメ起おコシ2〔スタート〕於正せい=北ほくヨリ1〔2いちだんめちうあう〕。東とう=北ほく=向かうニ次し=第だいニ填てんス0之これヲ。出いヅレバ0北きたヨリ入いリ0南みなみニ。出いヅレバ0東ひがしヨリ入いル0西にしニ。道みちニ有あレバ0礙さまたげ者。先まヅ南みなみノカタ退しりぞキテ而後のちニ進すすム。其それ較くらベテ2楊やう=輝きノ法はふニ1猶なホ稍やヤ易やすキ也（なり）。君きみ胡なんゾ不（ざ）ルヤ2一ひとタビ試ためサ10之これヲ。列れつ之（の）道みち。列れつ=列れつ之（の）道みち。至いたリテ0是ここニ略ほボ備そなハレリ矣〔3くゎんぜんにりかいした〕。更さらニ有あリ列れつ=列れつ=列れつ〔3さんじげんはいれつ〕。列れつ=列れつ=列れつ=列れつナル〔4よじげんはいれつ〕者（もの）。皆みな一いつ=理り〔2おなじりくつ〕也（なり）。

